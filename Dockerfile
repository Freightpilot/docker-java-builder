# Ubuntu 16.04 with Java 8 installed.

FROM ubuntu:16.04
MAINTAINER Jan-Hendrik Telke, https://github.com/jannhendrik
RUN \
    apt-get update -y && \
    apt-get upgrade -y

RUN \
    apt-get install -y software-properties-common && \
    apt-get install -y apt-transport-https && \
    apt-get install -y ca-certificates && \
    apt-get install -y curl && \
    apt-get update

RUN \
    apt-get install -y default-jre

#RUN \
#    add-apt-repository ppa:webupd8team/java -y && \
#    apt-get update && \
#    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
#    apt-get install -y oracle-java8-installer

RUN \
    apt-get install -y docker

RUN \
    apt-get install -y python3-pip
    
RUN \
    apt-get install -y nodejs && \
    apt-get install -y npm
    
RUN \ 
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
    apt-get update && \
    add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable" && \
    apt-get update && \
    apt-get install -y docker-ce
    
RUN \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && \ 
    apt-get install -y yarn
    
RUN docker --version && pip3 --version && nodejs -v && yarn -v && java -version

CMD docker --version && pip3 --version && nodejs -v && yarn -v && java -version